# Design Patterns in Python

- Course taken in Udemy: https://www.udemy.com/course/design-patterns-python

## The patterns

- Creational
- Structural
- Behavioral

## The SOLID Design principle

### Single Responsibility Principle (Separation of Concern)

