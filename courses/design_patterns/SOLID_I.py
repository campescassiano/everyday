# ISP

class Machine:
    def print(self, document):
        raise NotImplementedError

    def fax(self, document):
        raise NotImplementedError

    def scan(self, document):
        raise NotImplementedError

class MultifunctionPRinter(Machine):
    def print(self, document):
        pass

    def fax(self, document):f
        pass

    def scan(self, document):
        pass

class OldFashionedPrinter(Machine):
    def print(self, document):
        pass

    def fax(self, document):
        raise NotImplementedError('Printer cannot fax')

    def scan(self, document):
        raise NotImplementedError('Printer cannot scan')