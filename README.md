# Everyday

After a while, I decided to expose some stuff that I've solved for myself privately,
but those stuff could be helpful to someone else, or not at all.

So, I created this repo to keep track of problems and solutions I had to deal with
to stuff that I found interesting to share.

If these things were helpful to you somehow, then buy me a beer sometime. If not,
you still can buy me a beer ;)

## Contact

- E-mail: cassianocampes@gmail.com
- LinkedIn: https://www.linkedin.com/in/cassianocampes/
